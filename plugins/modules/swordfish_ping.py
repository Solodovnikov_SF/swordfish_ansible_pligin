#!/usr/bin/python
# -*- coding: utf-8 -*-

from __future__ import (absolute_import, division, print_function)

__metaclass__ = type


DOCUMENTATION = r'''
'''

EXAMPLES = r'''
'''

RETURN = r'''
'''

from plugins.module_utils.swordfish_module import SwordfishModule


class SwordfishPing(SwordfishModule):

    def __init__(self):
        super(SwordfishPing, self).__init__(
            supports_check_mode=True
        )

    def run(self):
        self.rest.get('redfish/v1/')
        self.exit_json(
            msg="Operation successful.",
            changed=False,
        )

def main():
    SwordfishPing()


if __name__ == '__main__':
    main()
