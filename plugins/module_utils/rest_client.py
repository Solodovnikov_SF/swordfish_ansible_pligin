from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

import json
from base64 import b64encode
from ansible.module_utils.urls import open_url
from ansible.module_utils.six.moves.urllib.parse import urlencode
from ansible.module_utils.six.moves.urllib.error import URLError, HTTPError
from plugins.module_utils.exception import RESTClientError, RESTAuthorizationError
from plugins.module_utils.models.drive_group import DriveGroup


class RestClient:
    def __init__(
        self,
        base_url,
        username=None,
        password=None,
        validate_certs=False,
        timeout=60,
    ):
        self._username = username
        self._password = password
        self._token = None
        self._session = None
        if "://" in base_url:
            self._protocol, self._host = base_url.split("://")
        else:
            self._protocol = "https"
            self._host = base_url
        self.validate_certs = validate_certs
        self.timeout = timeout

    def authorize(self, username=None, password=None, auth_method=None):
        self._username = username or self._username
        self._password = password or self._password

        try:
            response = self.post(
                'redfish/v1/SessionService/Sessions',
                body={'UserName': self._username, 'Password': self._password},
            ).headers
        except Exception as e:
            raise RESTAuthorizationError('{0}: {1}'.format(
                type(e).__name__, str(e)
            ))

        self._token = response['X-Auth-Token']
        self._session = response['Location']

    def make_request(
            self,
            path,
            method,
            query_params=None,
            body=None,
            headers=None,
    ):

        request_kwargs = {
            "follow_redirects": "all",
            "force_basic_auth": False,
            "headers": self._get_headers(),
            "method": method,
            "timeout": self.timeout,
            "use_proxy": True,
            "validate_certs": self.validate_certs,
        }
        if body:
            if isinstance(body, dict) or isinstance(body, list):
                request_kwargs["headers"]["Content-Type"] = "application/json"
                request_body = json.dumps(body)
            elif isinstance(body, bytes):
                request_kwargs["headers"]["Content-Type"] = "application/octet-stream"
                request_body = body
            else:
                raise RESTClientError(
                    "Unsupported body type: {0}".format(type(body)))
        else:
            request_body = None

        url = "{0}/{1}".format(self.base_url.rstrip("/"), path.lstrip("/"))
        if query_params:
            url += "?" + urlencode(query_params)

        if headers:
            request_kwargs["headers"].update(headers)

        try:
            response = open_url(url=url, data=request_body, **request_kwargs)
        except HTTPError as e:
            raise RESTClientError("Bad request: ".format(e.code))
        return response


    def get(self, path, query_params=None, headers=None):
        return self.make_request(path, method="GET", query_params=query_params, headers=headers)

    def post(self, path, body=None, headers=None):
        return self.make_request(path, method="POST", body=body, headers=headers)

    def delete(self, path, headers=None):
        return self.make_request(path, method="DELETE", headers=headers)


    def _get_headers(self):
        headers = {}
        if self._token:
            headers['X-Auth-Token'] = self._token

        return headers

    @property
    def base_url(self):
        return "{0}://{1}".format(self._protocol, self._host)

    def logout(self):  # type: () -> None
        if self._token:
            self.delete(self._session)
            self._token = None
            self._session = None

    def get_drive_groups(self):
        rv = []
        drive_groups_data = json.loads(self.get("redfish/v1/Storage").read())['Members']
        for data in drive_groups_data:
            rv.append(DriveGroup(client=self, path=data['@odata.id']))
        return rv
