from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

import json
from typing import List, Dict, Optional

class Drive:
    def __init__(self, client, drive_group, path):
        self._client = client
        self.drive_group = drive_group
        self.data = json.loads(client.get(path).read())

    @property
    def status(self):
        return self.data['Status']['Health']

    @property
    def name(self):
        return self.data['Name']

    @property
    def capacity(self):
        return self.data['CapacityBytes']

    @property
    def model(self):
        return self.data['Model']

    @property
    def serial_number(self):
        return self.data['SerialNumber']

    @property
    def slot(self):
        return self.data['PhysicalLocation']['PartLocation']['LocationType']

