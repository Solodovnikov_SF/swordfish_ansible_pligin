from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

import json
from typing import List, Dict, Optional, Union

from plugins.module_utils.models.drive import Drive


class DriveGroup:
    def __init__(self, client, path):
        self._client = client
        self._group_name = path
        self.data = json.loads(client.get(path).read())
        self.drives = []

        drive_data = self.data['Drives']
        for drive in drive_data:
            self.drives.append(Drive(client, self, drive['@odata.id']))
        volumes_data = json.loads((client.get(self.data['Volumes']['@odata.id']).read()))
        self.volumes = json.loads((client.get(volumes_data['Members'][0]['@odata.id']).read()))

    @property
    def name(self):
        return self.data['Name']

    @property
    def status(self):
        return self.data['Status']['Health']

    @property
    def drives_total(self):
        return len(self.drives)

    @property
    def drives_available(self):
        count = 0
        for drive in self.drives:
            if drive.status == "OK":
                count += 1
        return count

    @property
    def drives_failed(self):
        return self.drives_total - self.drives_available

    @property
    def drive_capacity(self):
        if len(self.drives) == 0:
            return 0
        return self.drives[0].capacity

    @property
    def capacity_total(self):
        return self.capacity_used + self.capacity_available

    @property
    def capacity_used(self):
        return self.volumes['Capacity']['Data']['ConsumedBytes']

    @property
    def capacity_available(self):
        return self.volumes['Capacity']['Data']['AllocatedBytes']
