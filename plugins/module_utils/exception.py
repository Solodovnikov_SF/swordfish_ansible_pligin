from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

class RESTClientError(Exception):
    pass

class RESTAuthorizationError(Exception):
    pass